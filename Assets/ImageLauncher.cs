﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageLauncher : MonoBehaviour {
    protected ImageLauncher() { }

    private bool closeOnClick = false;

    private static ImageLauncher _instance;
    public static ImageLauncher Instance
    {
        get
        { if(_instance == null) _instance = FindObjectOfType<ImageLauncher>();
        return _instance; 
        }
    }
    public static void ShowSprite(Sprite sprite, bool closeOnClick = false)
    {
        Instance.transform.GetChild(0).gameObject.SetActive(true);
        Instance.GetComponentInChildren<UnityEngine.UI.Image>().sprite = sprite;
        Instance.StartCoroutine(SetIsVisible(closeOnClick));
    }

    public static void Hide()
    {
        Instance.GetComponentInChildren<UnityEngine.UI.Image>().gameObject.SetActive(false);
    }

    static IEnumerator SetIsVisible(bool closeOnClick) {
        yield return new WaitForSeconds(0.5f);
        Instance.closeOnClick = closeOnClick;
    }

    void Update()
    {
        if (Instance.closeOnClick && Input.anyKey)
        {
            Hide();
            Instance.closeOnClick = false;
        }
    }
}
