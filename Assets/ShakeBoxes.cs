﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeBoxes : MonoBehaviour
{

    [SerializeField]
    private GameObject[] boxes;

    [SerializeField]
    private float jumpPower, shakeFrequency = 1;

    private float timer;


    // Use this for initialization
    void Start()
    {
        timer = shakeFrequency;
        GetNewBoxes();
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.JoystickButton1) && timer > shakeFrequency || Input.GetKeyDown(KeyCode.S) && timer > shakeFrequency || Input.GetKeyDown(KeyCode.DownArrow) && timer > shakeFrequency)
        {
            timer = 0;
            Camera.main.GetComponent<ShakeCamera>().StartShake(.1f, .4f);
            foreach (GameObject box in boxes)
            {
                box.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            }
        }
    }

    public void GetNewBoxes()
    {
        boxes = GameObject.FindGameObjectsWithTag("FishBox");

    }
}
