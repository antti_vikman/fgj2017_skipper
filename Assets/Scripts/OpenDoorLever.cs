using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//////////////////////////////////////////////////////////////////////////////////////////////////
//  Add this script to an object you want to be the lever   									//
//	Set stuff you dont want it to open the door with "Unbreakable" tag eg floors and seilings	//
//////////////////////////////////////////////////////////////////////////////////////////////////

public class OpenDoorLever : MonoBehaviour
{
	// whether triggerObj is within the lever collider so door can be opened
	bool triggerObjInRange = false;
	// the time in seconds between each lever trigger
	public float timeBetweenActions = 15f;
	float timer;

	private GameObject collidingObj;

	// whether collidinObj is within the trigger collider and can cause lever to trigger
	bool collidinObjInRange = false;

	bool isDoorOpen = false;

	// assign lever to open this door
	public GameObject doorToOpen;
	private Transform doorClosed;
	private Transform doorOpen;

	void Awake()
	{
		// doorClosed = doorToOpen.transform.Find("closed").gameObject;
		// doorOpen = doorToOpen.transform.Find("open").gameObject;

		doorClosed = doorToOpen.transform.GetChild(0);
		doorOpen = doorToOpen.transform.GetChild(1);

		doorClosed.gameObject.SetActive(true);
		doorOpen.gameObject.SetActive(false);
	}


	void Update()
	{
		// add the time since Update was last called to the timer
        timer += Time.deltaTime;

        // if the timer exceeds the time between actions and triggerObj is in range
        if(timer >= timeBetweenActions && triggerObjInRange && isDoorOpen == false)
        {
            // open the assigned doorToOpen
            OpenDoor();
        }
		else if(timer >= timeBetweenActions && triggerObjInRange && isDoorOpen == true)
		{
			// open the assigned doorToOpen
			CloseDoor();
		}
	}

	void OpenDoor()
	{
		// reset the timer
		timer = 0f;
		doorClosed.gameObject.SetActive(false);
		doorOpen.gameObject.SetActive(true);
	}


	void CloseDoor()
	{
		// reset the timer
		timer = 0f;
		doorClosed.gameObject.SetActive(true);
		doorOpen.gameObject.SetActive(false);
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		collidingObj = col.gameObject;

		// if the entering collider is the targetObj
		if(collidingObj.tag != "Unbreakable")
		{
			// the collidingObj is in range
			triggerObjInRange = true;
		}
	}


	void OnTriggerExit2D(Collider2D col)
	{
		collidingObj = col.gameObject;

		// if the exiting collider is the targetObj
		if(collidingObj.tag != "Unbreakable")
		{
			// the collidingObj is in range
			triggerObjInRange = false;
		}
	}
}
