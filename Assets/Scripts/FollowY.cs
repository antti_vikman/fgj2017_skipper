﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowY: MonoBehaviour {


    public Transform target;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y +2.5f, -10), speed*Time.deltaTime);
	}
}
