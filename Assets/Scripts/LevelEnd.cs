﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnd : MonoBehaviour
{

    [SerializeField]
    private GameObject[] boxes;

    private GameObject[] boxHPSet;

    public GameObject[] levels;

    private int currentLevel;

    private int finishedBoxes;

    DialogBox dialog;

    bool finished, moving;

    public ShakeBoxes shakeBoxes;
    public Sprite tutorialScreen;


    private float timer;


    // Use this for initialization
    void Start()
    {
        timer = 0;
        currentLevel = 0;
        Reset();
        ImageLauncher.ShowSprite(tutorialScreen, true);
        GameObject.Find("Boat").GetComponent<MumbleGenerator>().Reset();
    }

    void Update()
    { 

        if (!finished && !moving)
            CheckBoxes();

        if (finished)
        {
            timer += Time.deltaTime;
            if (Input.anyKey && timer > 2)
            {
                if(currentLevel < levels.Length)
                NewLevel();
                else 
                {
                    SceneManager.LoadScene(0);
                }
            }
        }

        if(Input.GetKey(KeyCode.Escape))
        {
         SceneManager.LoadScene(0);
        }
    }


    void CheckBoxes()
    {
        finishedBoxes = 0;
        foreach (GameObject box in boxes)
        {
            if (box.GetComponent<BoxState>().finished || box.GetComponent<BoxState>().destroyed)
            {
                finishedBoxes++;
            }
        }
        if (finishedBoxes >= boxes.Length)
        {
            print("Sillälailla!");
            finished = true;
            Finish();
            moving = true;
        }
    }

    void Finish()
    {
        dialog = FindObjectOfType<DialogBox>();
        dialog.RenderScoreView();
        dialog.Show();
        GameObject.Find("Boat").GetComponent<MumbleGenerator>().StopSpeech();
        System.Random rnd = new System.Random();
        gameObject.GetComponent<AudioArray>().PlayClip("level_end" + (rnd.Next(7) + 1));
    }

    void NewLevel()
    {
        finished = false;
        levels[currentLevel].SetActive(false);
        currentLevel++;
        Reset();
        levels[currentLevel].SetActive(true);


        levels[currentLevel].SetActive(true);
        dialog.Hide();
        moving = false;
        timer = 0;
        GameObject.Find("Boat").GetComponent<MumbleGenerator>().Reset();
    }

    void Reset()
    {
        finished = moving = false;
        foreach (GameObject level in levels)
        {
            level.SetActive(false);
        }
        levels[currentLevel].SetActive(true);

        shakeBoxes.GetNewBoxes();
        boxes = GameObject.FindGameObjectsWithTag("FishBox");
    }
}
