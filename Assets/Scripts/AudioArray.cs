﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioState {
	paused,
	playing,
	fadingOut,
	fadingIn
}


[System.Serializable]
public class ClipObject {
	public string key;
	public AudioClip clip;
	public bool loop;
	public bool playFromStart;
	public float fadeTime;
	public AudioSource _audioSource;
	public AudioState _audioState;
}

public class AudioArray: MonoBehaviour {

	public ClipObject[] clipObjects;

	void Awake () {
		for (int i = 0; i < clipObjects.Length; ++i) {
			ClipObject cobj = clipObjects[i];
			cobj._audioSource = gameObject.AddComponent<AudioSource>();
			cobj._audioSource.clip = cobj.clip;
			cobj._audioSource.loop = cobj.loop;
			cobj._audioSource.volume = .0f;
			cobj._audioSource.playOnAwake = false;
			cobj._audioState = AudioState.paused;
			if (cobj.playFromStart)
				PlayClip(cobj.key);
		}
	}
	
	void Update () {
		float dt = Time.deltaTime;
		for (int i = 0; i < clipObjects.Length; ++i) {
			ClipObject cobj = clipObjects[i];
			switch (cobj._audioState) {
			case AudioState.fadingIn:
				cobj._audioSource.volume = Mathf.Clamp(cobj._audioSource.volume + (dt/cobj.fadeTime), .0f, 1.0f);
				if (cobj._audioSource.volume >= 1.0f) cobj._audioState = AudioState.playing;
				break;
			case AudioState.fadingOut:
				cobj._audioSource.volume = Mathf.Clamp(cobj._audioSource.volume - (dt/cobj.fadeTime), .0f, 1.0f);
				if (cobj._audioSource.volume <= .0f) {
					cobj._audioState = AudioState.paused;
					cobj._audioSource.Stop();
				}
				break;
			case AudioState.playing:
				if (!cobj._audioSource.isPlaying)
					cobj._audioState = AudioState.paused;
				break;
			default:
				break;
			}
		}
	}

	public ClipObject GetClip(string key) {
		for (int i = 0; i < clipObjects.Length; ++i) {
			ClipObject cobj = clipObjects[i];
			if (cobj.key.Equals(key)) return cobj;
		}
		ClipObject ret = new ClipObject();
		ret.key = "null";
		return ret;
	}

	public void PauseAllClips() {
		for (int i = 0; i < clipObjects.Length; ++i) {
			ClipObject cobj = clipObjects[i];
			cobj._audioState = AudioState.fadingOut;
		}
	}

	public void PauseClip(string key) {
		ClipObject cobj = GetClip(key);
		cobj._audioState = AudioState.fadingOut;
	}

	public void PlayClip(string key) {
		ClipObject cobj = GetClip(key);
		cobj._audioState = AudioState.fadingIn;
		if (!cobj._audioSource.isPlaying)
			cobj._audioSource.Play();
	}
}
