﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherSystem: MonoBehaviour {

	public GameObject[] waveObjects;
	public float baseAmp;
	public float baseFreq;
	public float weatherOffset;
	public float weatherFreq;
	public float ampFactor;
	public float freqFactor;

	private WaveSystem[] _waveSystems;
	private float _weatherState;

	void Start () {
		_waveSystems = new WaveSystem[waveObjects.Length];
		for (int i = 0; i < waveObjects.Length; ++i) {
			WaveSystem waveSystem = waveObjects[i].GetComponent<WaveSystem>();
			_waveSystems[i] = waveSystem;
			waveSystem.waveAmp = baseAmp;
			waveSystem.waveFreq = baseFreq;
		}
		_weatherState = weatherOffset;
	}
	
	void Update () {
		float dt = Time.deltaTime;

		_weatherState += dt*360.0f*weatherFreq;
		if (_weatherState > 180.0f) _weatherState -= 360.0f;

		WaveSystem waveSystem0 = _waveSystems[0];
		waveSystem0.waveAmp = baseAmp + Mathf.Sin(Mathf.Deg2Rad*_weatherState)*ampFactor;
		waveSystem0.waveFreq = baseFreq + Mathf.Sin(Mathf.Deg2Rad*_weatherState)*freqFactor;

		for (int i = 1; i < _waveSystems.Length; ++i) {
			WaveSystem waveSystemN = _waveSystems[i];
			waveSystemN.waveAmp = waveSystem0.waveAmp;
			waveSystemN.waveFreq = waveSystem0.waveFreq;
		}
	}
}