﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    enum LoadTrigger
    {
        DELAY,
        ON_CALL
    }

    [SerializeField]
    string target;

    [SerializeField]
    LoadTrigger trigger;

    [SerializeField]
    float timeDelay;

    float startTime;

    void Start () {
		if(trigger == LoadTrigger.DELAY)
        {
            startTime = Time.time;
        }
	}
	
	void Update () {
		if(trigger == LoadTrigger.DELAY && (Time.time - startTime) > timeDelay)
        {
            SceneManager.LoadScene(target);
        }
	}

    public void LoadScene()
    {
        if (trigger == LoadTrigger.ON_CALL)
        {
            SceneManager.LoadScene(target);
        }
    }
}
