﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager> {

    protected GameManager() { }

    int _currentLevel;    
    public static int CurrentLevel
    {
        get
        {
            return GameManager.Instance._currentLevel;
        }
        set
        {
            GameManager.Instance._currentLevel = value;
        }
    }

    public static Dictionary<string, ObjectScore.ScoreEntity> CountScore()
    {
        Dictionary<string, ObjectScore.ScoreEntity> scoreTable = new Dictionary<string, ObjectScore.ScoreEntity>();
        foreach(var scoreObject in FindObjectsOfType<ObjectScore>())
        {
            var score = scoreObject.GetScore();
            if (!scoreTable.ContainsKey(score.scoreGroup))
            {
                scoreTable.Add(score.scoreGroup, score);
            } else
            {
                var updatedScore = scoreTable[score.scoreGroup];
                updatedScore.maxScore += score.maxScore;
                updatedScore.currentScore += score.currentScore;
                scoreTable[score.scoreGroup] = updatedScore;

            }
        }
        return scoreTable;
    }

    public void DebugScore()
    {
        foreach(var val in GameManager.CountScore().Values)
        {
            Debug.Log(val.scoreGroup + ": " + val.currentScore + "/" + val.maxScore);
        }
    }
}
