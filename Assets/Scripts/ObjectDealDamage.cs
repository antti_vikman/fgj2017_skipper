using UnityEngine;
using System.Collections;

///////////////////////////////////////////////////////////////////////////
//  Add this script to an object you want to do damage to stuff on hit   //
//	Set stuff you dont want to break with tag "Unbreakable"				 //
///////////////////////////////////////////////////////////////////////////

public class ObjectDealDamage : MonoBehaviour
{
	// the time in seconds between each attack
    public float timeBetweenDamage = 0.2f;
	// the amount of health taken away per attack, placeholder calculate with velocity
    public float damageAmount = 10;


	public GameObject targetObj;
	public ObjectHealth targetObjHealth;


	// whether targetObj is within the trigger collider and can be damaged
    bool targetObjInRange = false;
    float timer;


	void OnCollisionEnter2D(Collision2D col)
	{
		targetObj = col.gameObject;

		// if the entering collider is the targetObj
		if(targetObj.tag != "Unbreakable" && timer >= timeBetweenDamage)
		{
		    // the targetObj is in range
		    targetObjInRange = true;
			targetObjHealth = targetObj.GetComponent<ObjectHealth>();
            DamageTargObj();

            // ???????????????????????????????????????????????????????????????????????????
            // do checks for velocity somewhere, velocity score to compare ???????????????
            // ???????????????????????????????????????????????????????????????????????????
        }
    }


	void OnCollisionExit2D(Collision2D col)
	{
		targetObj = col.gameObject;

		// if the exiting collider is the targetObj
		if(targetObj.tag != "Unbreakable")
		{
		    // the targetObj is in range
		    targetObjInRange = false;
		}
	}


    void Update ()
    {
        // add the time since Update was last called to the timer
        timer += Time.deltaTime;

        // if the timer exceeds the time between attacks, targetObj is in range and targetObj is alive
        // if(timer >= timeBetweenDamage && targetObjInRange && targetObjHealth.currentHealth > 0)
        if(timer >= timeBetweenDamage && targetObjInRange)
        {
            // do dammage to targetObj
       //     DamageTargObj();
        }

        // if thisObj has zero or less health
        // if(thisObjHealth.currentHealth <= 0)
        // {
        //     // do something xD
        // }
    }


    void DamageTargObj ()
    {
        // reset the timer
        timer = 0f;

        // if the targetObj has health to lose
        if(targetObjHealth.currentHealth > 0)
        {
            // damage the targetObj
            targetObjHealth.TakeDamage(damageAmount);
        }
        else
        {
            targetObjHealth = null;
        }
    }
}
