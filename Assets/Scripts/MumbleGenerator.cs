﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SeriousnessLevel {
	public string audioKey;
	public float lowBound;
	public float highBound;
}

public class MumbleGenerator: MonoBehaviour {

	public float calmingTerm;
	public float seriousnessVelocityFactor;
	public float seriousnessAngleFactor;
	public float maxSeriousness;
	public SeriousnessLevel[] seriousnessLevels;

	private ShipWavePhysics _shipWavePhysics;
	private AudioArray _audioArray;

	private float _seriousness;
	private int _curSeriousnessLevel;
	private bool _silent;

	void Start () {
		_shipWavePhysics = GetComponent<ShipWavePhysics>();
		_audioArray = GetComponent<AudioArray>();
		_seriousness = .0f;
		_curSeriousnessLevel = 0;
		_silent = false;
		Reset();
	}

	public void StopSpeech() {
		_silent = true;
		_audioArray.PauseClip("roope_mumble_level1");
		_audioArray.PauseClip("roope_mumble_level2");
		_audioArray.PauseClip("roope_mumble_level3");
		_audioArray.PauseClip("roope_mumble_level4");
	}

	public void Reset() {
		StopSpeech();
		_silent = false;
		_seriousness = .0f;
		_curSeriousnessLevel = 0;
		Debug.Log(_curSeriousnessLevel);
		Debug.Log(seriousnessLevels[_curSeriousnessLevel].audioKey);
		_audioArray.PlayClip(seriousnessLevels[_curSeriousnessLevel].audioKey);
		_audioArray.PlayClip("ship");
	}
	
	void Update () {
		if (_silent) {
			StopSpeech();
		}
		else {
			float a = Mathf.Clamp(_shipWavePhysics.GetYVelocity(), .0f, maxSeriousness);
			float b = Mathf.Abs(_shipWavePhysics.GetAngle());
			_seriousness = Mathf.Clamp(_seriousness + a*seriousnessVelocityFactor + b*seriousnessAngleFactor - Mathf.Log(_seriousness + 1, 10)*calmingTerm, .0f, maxSeriousness);
			SeriousnessLevel serLev = seriousnessLevels[_curSeriousnessLevel];
			if (_seriousness < serLev.lowBound) {
				_audioArray.PauseAllClips();
				_silent = false;
				serLev = seriousnessLevels[--_curSeriousnessLevel];
				_audioArray.PlayClip(serLev.audioKey);
			}
			else if (_seriousness > serLev.highBound) {
				_audioArray.PauseAllClips();
				_silent = false;
				serLev = seriousnessLevels[++_curSeriousnessLevel];
				_audioArray.PlayClip(serLev.audioKey);
			}
		}
	}
}
