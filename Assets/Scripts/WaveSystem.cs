﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSystem: MonoBehaviour {

	public int waterResolution;
	public float waterWidth;

	public float waveFreq;
	public float waveAmp;
	public float causalityFactor;
	public Material waterMaterial;

	private MeshFilter _meshFilter;
	private MeshRenderer _meshRenderer;
	private Vector3[] _waterPoints;
	private Vector3[] _vertices;
	private float _angleAccum;

	void UpdateWaterPointObjects() {
		for (int i = 0; i < waterResolution; ++i) {
			_vertices[i + waterResolution] = _waterPoints[i];
		}
		_meshFilter.mesh.vertices = _vertices;
	}

	Mesh CreateWaveMesh() {
		Mesh mesh = new Mesh();
		mesh.name = "WaveMesh";
		_vertices = new Vector3[waterResolution*2];
		for (int i = 0; i < waterResolution; ++i) {
			_vertices[i] = new Vector3(i*(waterWidth/waterResolution) - (waterWidth/2.0f), -50.0f, .0f);
			_vertices[i + waterResolution] = _waterPoints[i];
		}
		int[] triangles = new int[(waterResolution - 1)*6];
		for (int i = 0; i < waterResolution - 1; ++i) {
			triangles[i*6 + 0] = i;
			triangles[i*6 + 1] = i + waterResolution;
			triangles[i*6 + 2] = i + waterResolution + 1;
			triangles[i*6 + 3] = i;
			triangles[i*6 + 4] = i + waterResolution + 1;
			triangles[i*6 + 5] = i + 1;
		}
		mesh.vertices = _vertices;
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		return mesh;
	}

	void Start () {
		_angleAccum = .0f;
		_waterPoints = new Vector3[waterResolution];

		for (int i = 0; i < waterResolution; ++i) {
			Vector3 pos = new Vector3(i*(waterWidth/waterResolution) - (waterWidth/2.0f), .0f, .0f);
			_waterPoints[i] = pos;
		}
		_meshFilter = gameObject.AddComponent<MeshFilter>();
		_meshRenderer = gameObject.AddComponent<MeshRenderer>();
		_meshRenderer.material = waterMaterial;
		_meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		_meshRenderer.receiveShadows = false;
		_meshFilter.mesh = CreateWaveMesh();
	}

	void Update () {
		float dt = Time.deltaTime;
		_angleAccum += dt*waveFreq;
		if (_angleAccum > 360.0f) _angleAccum -= 360.0f;
		for (int i = 0; i < waterResolution - 1; ++i) {
			Vector3 p0 = _waterPoints[i];
			Vector3 p1 = _waterPoints[i + 1];
			p0.y += (p1.y - p0.y)*causalityFactor;
			_waterPoints[i] = p0;
		}
		Vector3 pN = _waterPoints[waterResolution - 1];
		pN.y += (Mathf.Sin(Mathf.Deg2Rad*_angleAccum)*waveAmp - pN.y)*causalityFactor;
		_waterPoints[waterResolution - 1] = pN;

		UpdateWaterPointObjects();
	}

	public float GetHeightAtX(float x) {
		x = ((x + waterWidth/2.0f)/waterWidth)*(waterResolution - 1);
		int i = (int)x;
		float a = x - Mathf.Floor(x);
		float h0 = _waterPoints[i].y;
		float h1 = _waterPoints[i + 1].y;
		return h0 + (h1 - h0)*a;
	}

	public float GetAngleAtX(float x) {
		x = ((x + waterWidth/2.0f)/waterWidth)*(waterResolution - 1);
		int i = (int)x;
		float h0 = _waterPoints[i].y;
		float h1 = _waterPoints[i + 1].y;
		float h = h1 - h0;
		float w = waterWidth/waterResolution;
		return Mathf.Rad2Deg*Mathf.Atan(h/w);
	}
}