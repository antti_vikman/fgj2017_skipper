﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IScoreRationProvider))]
public class ObjectScore : MonoBehaviour {

    enum Rounding
    {
        NONE,
        TO_INT_FLOOR,
        TO_INT_CELING
    }

    public interface IScoreRationProvider
    {
        float GetScoreRation();
    }

    [System.Serializable]
    public struct ScoreEntity
    {
        public string scoreGroup;
        public float maxScore;
        public float currentScore;
    }

    [SerializeField]
    private ScoreEntity score;

    [SerializeField]
    private Rounding rounding;

    public ScoreEntity GetScore()
    {
        var newScore = score.maxScore * GetComponent<IScoreRationProvider>().GetScoreRation();
        switch (rounding) {
            case Rounding.TO_INT_FLOOR:
                newScore = Mathf.FloorToInt(newScore);
                break;
            case Rounding.TO_INT_CELING:
                newScore = Mathf.CeilToInt(newScore);
                break;
        }
        score.currentScore = newScore;
        return score;
    }   	
}
