﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipWavePhysics: MonoBehaviour {

	public GameObject waveSystemObject;
	public float positionalInertia;
	public float angularInertia;

	private WaveSystem _waveSystem;
	private Vector3 _vel;
	private float _angVel;

    public float angleOut;
    public BoatMovement boatMovement;

	void Start () {
		_waveSystem = waveSystemObject.GetComponent<WaveSystem>();
        boatMovement = GetComponent<BoatMovement>();
	}
	
	void Update () {
		Vector3 pos = transform.localPosition;
		Vector3 newPos = new Vector3(pos.x, _waveSystem.GetHeightAtX(pos.x), pos.z);
		float posFactor = 1.0f/(1 + positionalInertia);
		_vel = (newPos - pos)*posFactor;
		transform.localPosition = pos + _vel;

		float ang = transform.localEulerAngles.z;
		if (ang > 180.0f) ang -= 360.0f;
		if (ang < -180.0f) ang += 360.0f;
		float newAng = _waveSystem.GetAngleAtX(pos.x);
		float angFactor = 1.0f/(1 + angularInertia);
		_angVel = (newAng - ang)*angFactor;
		transform.localEulerAngles = new Vector3(.0f, .0f, ang + _angVel);
		if (boatMovement != null) boatMovement.ShipControl(ang + _angVel);
	}

	public float GetYVelocity() {
		return _vel.y;
	}

	public float GetAngularVelocity() {
		return _angVel;
	}

	public float GetAngle() {
		if (transform.localEulerAngles.z > 180.0f)
			return transform.localEulerAngles.z - 360.0f;
		else
			return transform.localEulerAngles.z;
	}
}