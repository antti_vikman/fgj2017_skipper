﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour
{

    private float angle;

    float horizontal, startX;



    private ShipWavePhysics sWP;

    [SerializeField]
    private float rotationSpeed, defRotationSpeed = 2, controlMag = 15;


    void Start()
    {

      
        sWP = GetComponent<ShipWavePhysics>();

        startX = transform.position.x;

        rotationSpeed = 0;
    }

    void Update()
    {
        transform.position = new Vector2(startX, transform.position.y);
    }


    public void ShipControl(float shipAngle)
    {
        horizontal = Input.GetAxis("Horizontal");

        if (horizontal < 0)
        {
            rotationSpeed = controlMag * horizontal * -1;
            transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z + Time.deltaTime * rotationSpeed);
        }
        else if (horizontal > 0)
        {
            rotationSpeed = controlMag * horizontal * -1;

            transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z + Time.deltaTime * rotationSpeed);
        }
        else
        {
            /*if (rotationSpeed != 0)
            {
                transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z + Time.deltaTime * rotationSpeed);

                rotationSpeed = Mathf.MoveTowards(rotationSpeed, 0, Time.deltaTime);
            }
            else*/
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.x, transform.rotation.y, shipAngle + rotationSpeed), Time.deltaTime * defRotationSpeed);
        }

    }


    public float BoatJump(float orig)
    {
        float sinkAmount = orig - 30;
        float y = orig;  
        y = Mathf.MoveTowards(y, sinkAmount , 10*Time.deltaTime);

        return orig;
    }

}
