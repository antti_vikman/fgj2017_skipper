﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogBox : MonoBehaviour {

    [SerializeField]
    private Vector3 showPosition, hidePosition;
    [SerializeField]
    private float movementTime = 2, lerpTime = 0.05f;

    public void Show()
    {
        StartCoroutine("MoveTo", showPosition);
    }

    public void Hide()
    {
        StartCoroutine("MoveTo", hidePosition);
    }

    IEnumerator MoveTo(Vector3 destination)
    {
        float timeEllapsed = 0;
        Vector3 origin = transform.position;

        while (timeEllapsed < movementTime)
        {
            transform.position = Vector3.Lerp(origin, destination, timeEllapsed / movementTime);
            timeEllapsed += lerpTime;
            yield return new WaitForSeconds(lerpTime);
        }
    }

    public void RenderScoreView()
    {
        //foreach (var child in transform)
        //{
        //    var childTransform = child as RectTransform;
        //    childTransform.gameObject.SetActive(false);
        //}
        var scoreBoard = FindObjectOfType<ScoreBoard>();
        scoreBoard.gameObject.SetActive(true);
        scoreBoard.RenderScore();
    }
}
