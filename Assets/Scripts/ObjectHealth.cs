using UnityEngine;

////////////////////////////////////////////////////////////////////////////////////
//  Add this script to an object you want to be breakable when dealt damage to   ///
////////////////////////////////////////////////////////////////////////////////////


public class ObjectHealth : MonoBehaviour, ObjectScore.IScoreRationProvider
{
    public float startingHealth = 10;
    public float currentHealth;
    // The amount reduced from the player's score when the object dies or something
    public int scoreValue = 10;
    public AudioClip deathClip;

    // AudioSource objAudio;
    public ParticleSystem hitParticles;
    BoxCollider2D boxCollider2D;
    // Whether the object is destroyed
    bool isDead;


    void Awake()
    {
        // objAudio = GetComponent <AudioSource> ();
        // hitParticles = GetComponentInChildren <ParticleSystem> ();
        boxCollider2D = GetComponent<BoxCollider2D>();

        // Setting the current health when the object first spawns
        currentHealth = startingHealth;
    }


    public void TakeDamage(float amount)
    {
        // If the object is destroyed
        if (isDead)
            // no need to take damage so exit the function
            return;

        // lay the damage sound effect
        // objAudio.Play ();

        // reduce the current health by the amount of damage taken
        currentHealth -= amount;

        // and play the particles
        // hitParticles.Play();

        // If the current health is less than or equal to zero
        if (currentHealth <= 0)
        {
            // all the destrouy stuff
            Death();
        }
    }

    // add mayheim here
    void Death()
    {

        // The object is dead
        isDead = true;
        if (GetComponent<BoxState>())
        {
            GetComponent<BoxState>().destroyed = true;
           // GetComponent<Rigidbody2D>().drag = 3;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            GetComponent<SpriteRenderer>().enabled = false;

        }

        // Turn the collider into a trigger so stuff can pass through it
        //  boxCollider2D.isTrigger = true;
        //Destroy (gameObject);
        hitParticles.Play();

        

        gameObject.layer = 8;

        // add here some stuff like a broken object for visuals or something

        // Change the audio clip of the audio source to the death clip and play it
        // objAudio.clip = deathClip;
        // objAudio.Play ();
    }

    public float GetScoreRation()
    {
        return currentHealth / startingHealth;
    }
}
