﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton: MonoBehaviour {

	void Start () {
	}
	
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, 100.0f)) {
				GameObject gobj = hit.collider.gameObject;
				if (gobj == gameObject) {
					gameObject.GetComponent<SceneLoader>().LoadScene();
				}
			}
		}
	}
}
