﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{

    [SerializeField]
    private GameObject boxTemplate;

    [SerializeField]
    private string scoreGroupName;

    [SerializeField]
    private Sprite successSprite;

    [SerializeField]
    private float succesAnimationStartDelay, succesAnimationDelay;

    private List<GameObject> boxes;

    void Start()
    {
        boxTemplate.SetActive(false);
        boxes = new List<GameObject>();
    }

    public void RenderScore()
    {
        Clear();
        foreach (var val in GameManager.CountScore().Values)
        {
            if(val.scoreGroup == scoreGroupName)
            {
                GenerateBoxes(Mathf.CeilToInt(val.maxScore));
                StartCoroutine(SetBoxes(Mathf.CeilToInt(val.currentScore)));
            }
        }
    }

    private void Clear()
    {
        boxes.Clear();

        foreach (var child in transform)
        {
            var childTransform = child as RectTransform;
            if (childTransform.gameObject.active)
            {
                Object.Destroy(childTransform.gameObject);
            }
        }
    }

    private void GenerateBoxes(int maxCount)
    {
        for(int i = 0; i < maxCount; i++)
        {
            var newBox = Object.Instantiate(boxTemplate, transform);
            newBox.SetActive(true);
            newBox.name = "Box" + (i + 1);
            boxes.Add(newBox);
        }
    }

    private IEnumerator SetBoxes(int successCount)
    {
        yield return new WaitForSeconds(succesAnimationStartDelay);
        for (int i = 0; i < successCount; i++)
        {
            StartCoroutine(AnimateShow(boxes[i], i * succesAnimationDelay));
        }
    }

    private IEnumerator AnimateShow(GameObject box, float delay)
    {
        yield return new WaitForSeconds(delay);
        box.GetComponent<UnityEngine.UI.Image>().sprite = successSprite;
        box.GetComponent<Animator>().SetTrigger("Bounce");
    }
}
