﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxState : MonoBehaviour
{ 
    public bool finished, destroyed;

    void Start()
    {
        finished = false;
        destroyed = false;
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if(!destroyed && col.gameObject.tag == "EndLevel")
        {
            finished = true;
        }
    }

}
